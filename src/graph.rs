#[derive(Debug, PartialEq)]
struct Edge {
    from: super::Point,
    to: super::Point,
}
pub struct Path {
    path: Vec<Edge>,
}

impl Path {
    pub fn from(points: Vec<super::Point>) -> Self {
        let first_iter = points.iter();
        let second_iter = points.iter().skip(1);
        let mut forward: Vec<Edge> = first_iter.zip(second_iter).map(|x| Edge{from: (x.0).clone(), to: x.1.clone()}).collect();
        let mut backward: Vec<Edge> = points.iter().rev().zip(points.iter().skip(1).rev()).map(|x| Edge{from: x.0.clone(), to: x.1.clone()}).collect();
        forward.append(&mut backward);
        Self {
            path: forward,
        }
    }
    pub fn merge_with(&mut self, other: &Self) {}
}

#[cfg(test)]
mod tests {
    use super::{Path, Edge};
    use super::super::Point;

    #[test]
    fn edges_creation_single() {
        let points: Vec<Point> = [(0.0,1.0), (0.0,0.0)].into_iter().map(|(a,o)| (*a, *o)).map(|(lat, lon)| Point{lat,lon}).collect();
        let path = Path::from(points.clone());
        assert_eq!(path.path, vec![Edge{from: points[0].clone(), to: points[1].clone()}, Edge{from: points[1].clone(), to: points[0].clone()}])
    }

    #[test]
    fn edges_creation_odd() {
        let points: Vec<Point> = [(0.0,1.0), (0.0,0.0), (2.0,2.0)].into_iter().map(|(a,o)| (*a, *o)).map(|(lat, lon)| Point{lat,lon}).collect();
        let path = Path::from(points.clone());
        assert_eq!(path.path, vec![Edge{from: points[0].clone(), to: points[1].clone()}, Edge{from: points[1].clone(), to: points[2].clone()}, Edge{to: points[0].clone(), from: points[1].clone()}, Edge{to: points[1].clone(), from: points[2].clone()}])
    }
}
