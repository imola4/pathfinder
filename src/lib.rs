use errors::Error;
use rayon::prelude::*;

mod errors;
mod graph;

#[derive(Debug, PartialEq, Clone)]
pub struct Point {
    pub lat: f64,
    pub lon: f64,
}

pub fn find_path(from: Point, to: Point, ways: Vec<Vec<Point>>) -> Result<Vec<Point>, errors::Error> {
    let mut ways_graphs = ways.into_iter().map(|x| graph::Path::from(x)).collect::<Vec<graph::Path>>();
    let mut final_graph = ways_graphs.pop().unwrap();
    for way in ways_graphs {
        final_graph.merge_with(&way);
    }

    Err(errors::Error::PathNotFound)
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
